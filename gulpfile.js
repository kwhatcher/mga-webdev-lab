'use strict';
// =======================================================================
// Gulp Plugins
// =======================================================================
var gulp = require('gulp'),
    nunjucksRender = require('gulp-nunjucks-render'),
    connect = require('gulp-connect'),
    open = require('gulp-open'),
    less = require('gulp-less');

// =======================================================================
// ENV Vars
// =======================================================================
var dist = 'lab/2/2.2'; //Set this t


// =======================================================================
// CSS Task (Generate CSS file from Less source files)
// =======================================================================
gulp.task('less', function() {
    return gulp.src(['src/style.less'])
        .pipe(less())
        .pipe(gulp.dest(dist));
});

// =======================================================================
// Index Task (Generate index.html from template)
// =======================================================================
gulp.task('index', function() {
    // Gets .html and .nunjucks files in pages
    return gulp.src('src/index.nunjucks')
    // Renders template with nunjucks
        .pipe(nunjucksRender({
            path: ['src/templates']
        }))
        // output files in app folder
        .pipe(gulp.dest(dist))
});

// =======================================================================
// Connect Task (starts dev webserver on local host)
// =======================================================================
gulp.task('connect', ['less', 'index'], function () {
    connect.server({
        root: dist,
        port: 4000
    })
});

// =======================================================================
// Browser Task (opens default browser)
// =======================================================================
gulp.task('uri', ['connect'], function(){
    gulp.src(__filename)
        .pipe(open({uri: 'http://localhost:4000'}));
});

// =======================================================================
// Images Task
// =======================================================================
gulp.task('images', function() {
    return gulp.src('src/img/*')
        .pipe(gulp.dest(dist.concat('/img/')));
});


// =======================================================================
// Default Task (called when you run `gulp` from cli)
// =======================================================================
gulp.task('default', ['less', 'index', 'connect', 'uri']);