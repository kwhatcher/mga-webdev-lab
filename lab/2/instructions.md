# Lab 2



## 2.1

Modify your Lab 1 submission and add images to your web page.  You can use some of the images provided or find/use any relevant images via the web about that are relevant (plumbing).  If this was real-life and not academic; you would not want to use images from Google Image search (you want to have usage rights).  
  
All images are grouped in their own folder  
Images and Links used for each the “Proud Member” links  
CSS sets a background image  
`<figure>` tag used  
Configure images w/ CSS  
HTML validator stays happy (green and no errors)  
CSS only exists in an external file  

## 2.2
  
Modify your Part 1 submission and add new HTML5 <div> tags to structure regions of the page.  Navigation and Main should make a 2 column layout.   
  
`<header>`, `<footer>`, `<nav>`, and main sections used   
Navigation and Main create a 2 column layout  
Floats cleared when necessary  
Start finalizing your design/brand  
HTML validator stays happy (green and no errors)  
CSS only exists in an external file  