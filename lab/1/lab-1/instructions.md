# ITEC 2380 - Lab 1

### Background

Keith McDonald Plumbing is a local business in desperate need of an updated website.  The website currently available (1/7/2016) was originally made in the early 2000’s (you can tell by the source code); it was revamped into Wix but the client still wants a better website.  It uses a very early version of HTML (and lots of syntax errors TBH).  In the labs this semester we will remake this website using HTML5 techniques in our own code.  

http://www.keithmcdonaldplumbing.com
Oldest Archive 12/2005
https://web.archive.org/web/20041206013618/http://www.keithmcdonaldplumbing.com
Newest Archive 9/2015
https://web.archive.org/web/20150928214214/http://keithmcdonaldplumbing.com

### Lab 1 - Part 1

Refer to the attached ZIP file.  In the Lab1.png screenshot I’ve shaded out much of the current page.  The unshaded region is going to our starting point to rebuilding the website with the Labs this semester.

Using the starter text provided in the Lab; create a web page w/ HTML5 such that the HTML Validator is happy (shows green and no errors).  You should be using a variety of elements to accomplish this.

I will want to see at least:

<div class=”wrapper”> used to contain the other elements
An id used
<p> tag used
Phrase elements such as <em> or <strong> used
A <span> used
< ul> or <ol> used
Refer to this part’s Lab video for clarification or ask in D2L discussions.

https://youtu.be/Wf95E_Pbsa0 - Walkthrough on HTML

https://youtu.be/LOf8OhkTzyo - Reflection on Lab Approach

### Lab 1 - Part 2

Modify your part 1 submission and add a stylesheet to your web page.  Use a color scheme generator (ex: http://www.colorsontheweb.com/colorwizard.asp) to choose a color scheme of your liking.

A chosen color scheme w/ color codes added as comments in top of CSS file
You are not locked into this scheme
If you do change them in future labs do change your comments too (to keep track)
<div class=”wrapper”> centers the content in browser with CSS
Your color scheme is implemented through CSS in your page
Do some of these CSS things to enhance your page to your liking:
Underline something
Bold something
Italicize something
Change Text and/or Background Colors
Adjust margin/padding
Change fonts or font-sizes
HTML validator stays happy (green and no errors)
CSS only exists in an external file
Refer to this part's Lab video for clarification or ask in D2L discussions.

https://youtu.be/Q1ZKgdWan_s - Walkthrough on CSS sheets

### Lab Grading Policy

Outcomes should be unique and reflect your design choices
Videos walkthroughs are only a guide and not the answer; your are highly encouraged to use your creativity in providing the client a good website using your own HTML, CSS, and JS.
I encourage you to use the validators to find errors in your code
https://validator.w3.org/ - I will take off for HTML errors
http://jigsaw.w3.org/css-validator/ - I will not take off for CSS errors as long as you’re using CSS correctly. Jigsaw is pretty strict IMHO but it can help you find some errors.
Lab should fulfill all required elements and be submitted as a .zip or .rar file to D2L Dropbox